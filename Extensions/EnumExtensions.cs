﻿using System;
using System.Linq;
using System.Reflection;

namespace KodSynaMaminojPodrugi.Extensions
{
    static class EnumExtensions
    {
        /// <summary>
        /// Returns the values of DisplayAttribute
        /// </summary>
        /// <param name="genericEnum"></param>
        /// <returns></returns>
        public static string GetName(this Enum genericEnum)
        {
            var genericEnumType = genericEnum.GetType();
            MemberInfo[] memberInfo = genericEnumType.GetMember(genericEnum.ToString());
            if ((memberInfo != null && memberInfo.Length > 0))
            {
                var attribs = memberInfo[0].GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.DisplayAttribute), false);
                if (attribs != null && attribs.Any())
                {
                    return ((System.ComponentModel.DataAnnotations.DisplayAttribute)attribs.ElementAt(0)).Name;
                }
            }
            return genericEnum.ToString();
        }
    }
}
