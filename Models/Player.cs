﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KodSynaMaminojPodrugi.Models
{
    class Player
    {
        public Player()
        {

        }

        public Player(string name, int score)
        {
            Name = name;
            Score = score;
        }

        public string Name { get; set; }

        public int Score { get; set; }
    }
}
