﻿namespace KodSynaMaminojPodrugi.Models
{
    internal class NameNumber
    {
        public NameNumber()
        {

        }

        public NameNumber(string name, int value)
        {
            Name = name;
            Value = value;
        }

        public int Value { get; set; }

        public string Name { get; set; }
    }
}
