﻿using KodSynaMaminojPodrugi.Models;
using System.Collections.Generic;
using System.Resources;

namespace KodSynaMaminojPodrugi.Services.Contracts
{
    internal interface IMenuService
    {
        /// <summary>
        /// Returns a list of the menu points
        /// </summary>
        /// <param name="resourceManager">active resource manager</param>
        /// <returns></returns>
        List<NameNumber> GetMenuPoints(ResourceManager resourceManager = null);
    }
}