﻿namespace KodSynaMaminojPodrugi.Services.Contracts
{
    internal interface IGameService
    {
        /// <summary>
        /// Returns the formatted string of the game rules
        /// </summary>
        string GetFormattedRulesString();

        /// <summary>
        /// Applies the word for the game
        /// </summary>
        /// <param name="mainWord">main word for the game</param>
        IGameService SetMainWord(string mainWord);
        
        /// <summary>
        /// Adds a new player to the game
        /// </summary>
        /// <param name="name">player's name</param>
        /// <returns></returns>
        IGameService AddPlayer(string name);

        /// <summary>
        /// Verifies if the user's word is valid for the game
        /// </summary>
        /// <param name="playerName">player's name</param>
        /// <param name="input">player's word</param>
        bool HandlePlayerInput(string playerName, string input);

        /// <summary>
        /// Returns the user's score
        /// </summary>
        /// <param name="playerName">plyer's name</param>
        /// <returns></returns>
        int GetPlayerScore(string playerName);


    }
}
