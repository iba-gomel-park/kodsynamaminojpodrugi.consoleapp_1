﻿using KodSynaMaminojPodrugi.Enums;
using KodSynaMaminojPodrugi.Exceptions;
using KodSynaMaminojPodrugi.Services;
using KodSynaMaminojPodrugi.Services.Contracts;
using System;
using System.Globalization;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace KodSynaMaminojPodrugi
{
    class Program
    {
        // constants definistion

        // static definistions
        private static ResourceManager _resourceManager;
        private static readonly ISettingService _settingService = new SettingService();
        private static readonly IGameService _gameService = new GameService();
        private static readonly IMenuService _menuService = new MenuService(_gameService, _settingService);
               
        /// <summary>
        /// Configures the app to start the game
        /// </summary>
        static void Initialize()
        {
            // set the encoding to handle cyrillic characters
            Console.OutputEncoding = Encoding.UTF8;

            // init the resource manager for the current system culture
            _resourceManager = GetActiveResources();
        }

        /// <summary>
        /// Returns a resource manager for the current system culture
        /// </summary>
        /// <returns></returns>
        static ResourceManager GetActiveResources()
        {
            if (CultureInfo.CurrentCulture.Name.Equals("ru-RU"))
                return new ResourceManager("KodSynaMaminojPodrugi.Resources.en", typeof(Program).Assembly);
            else
                return new ResourceManager("KodSynaMaminojPodrugi.Resources.ru", typeof(Program).Assembly);
        }

        /// <summary>
        /// Returns a resource manager for language
        /// </summary>
        /// <param name="language">the language the resource should be associated with</param>
        /// <returns></returns>
        static ResourceManager GetResources(Language language)
        {
            // you should use switch statement if there are more than 2 options
            if (language == Language.English)
                return new ResourceManager("KodSynaMaminojPodrugi.Resources.en", typeof(Program).Assembly);
            else
                return new ResourceManager("KodSynaMaminojPodrugi.Resources.ru", typeof(Program).Assembly);
        }

        /// <summary>
        /// Reads user's input and returns the value with the specified type
        /// </summary>
        /// <typeparam name="T">input type</typeparam>
        /// <param name="promptMessage">prompt message that must be shown before the input request</param>
        /// <returns></returns>
        static T AskUserForInput<T>(string promptMessage = null)
        {
            // go to the next line
            Console.WriteLine(Environment.NewLine);

            // show message if specified
            if(!string.IsNullOrWhiteSpace(promptMessage))
                Console.WriteLine(promptMessage);

            var input = Console.ReadLine();

            try
            {
                // return the read value as the specified type
                return (T)Convert.ChangeType(input, typeof(T));
            }
            catch
            {
                // add logging for the error
                throw new InvalidInputException(_resourceManager.GetString("InvalidInput"));
            }
        }

        /// <summary>
        /// Handles the menu point that user selected
        /// </summary>
        /// <param name="selectedPoint">selected menu point</param>
        static void HandleMenuPoint(MenuPoint selectedPoint)
        {
            switch (selectedPoint)
            {
                case MenuPoint.Start:
                    // handle the game cycle
                    break;
                case MenuPoint.Settings:
                    // show settings
                    break;
                case MenuPoint.Rules:
                    // show rules
                    break;
                default:
                    // handle the case
                    break;
            }

        }

        static async Task Main(string[] args)
        {
            // initialize the app configs
            Initialize();

            // use services or other classes to manage the app cycle
            
        }
    }
}
